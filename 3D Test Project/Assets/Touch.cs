using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Touch : MonoBehaviour, IDataPersistance
{
    int touchedGO;
    public TextMeshProUGUI touchPanelLvl1;
    public TextMeshProUGUI touchPanelLvl2;
    public TextMeshProUGUI touchPanelLvl3;
    public TextMeshProUGUI touchGameOver;
    public void LoadData(GameData data) {
        this.touchedGO = data.touchedGO;
    }
    public void SaveData(ref GameData data) {
        data.touchedGO = this.touchedGO;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        touchPanelLvl1.text = touchedGO.ToString();
        touchPanelLvl2.text = touchedGO.ToString();
        touchPanelLvl3.text = touchedGO.ToString();
        touchGameOver.text = touchedGO.ToString();
    }
    private void OnTriggerEnter(Collider other) {

        if (other.gameObject.name== "StartObject"||other.gameObject.tag=="PowerUp") {
            touchedGO++;
            Debug.Log("chocamos");

        }
        
    }
    
}
