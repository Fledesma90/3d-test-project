using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{
    [SerializeField]Score score;
    Timer timer;
    [SerializeField] GameObject level1Panel;
    [SerializeField] GameObject level2Panel;
    [SerializeField] GameObject level3Panel;
    // Start is called before the first frame update
    void Start()
    {
        score = FindObjectOfType<Score>();
        //reset timer so we see the timer from 0 in every level
        //and see the results at the end of the level
        timer = FindObjectOfType<Timer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (score.score>=100&& SceneManager.GetActiveScene().name == "Level1") {
            //TODO activate panel with results of gameobjects touched and time
            level1Panel.SetActive(true);

        }
        if (score.score>=200&& SceneManager.GetActiveScene().name == "Level2") {
            //TODO activate panel with results of gameobjects touched and time
            level2Panel.SetActive(true);
        }
        if (score.score>=400&& SceneManager.GetActiveScene().name == "Level3") {
            //todo activate panel of finishing game
            level3Panel.SetActive(true);
        }
    }

    public void ChangeToLevel2() {
        SceneManager.LoadScene("Level2");
        
    }
    public void ChangeToLevel3() {
        SceneManager.LoadScene("Level3");
        
    }

    public void Restart() {
        if (SceneManager.GetActiveScene().name == "Level1") {
            //todo put points to 0
            //todo put gotouched to 0
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (SceneManager.GetActiveScene().name == "Level2") {
            //put points to 100
            //todo put gotouched to 0
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (SceneManager.GetActiveScene().name == "Level3") {
            //todo put gotouched to 0
            //put points to 200
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        
    }

    public void Exit() {
        Application.Quit();
    }
}
