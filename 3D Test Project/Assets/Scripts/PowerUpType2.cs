using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PowerUpType2 : MonoBehaviour
{
    [SerializeField] int pointsToAdd2;
    // Start is called before the first frame update
    void Start() {
        if (SceneManager.GetActiveScene().name == "Level1") {
            pointsToAdd2 = 2;
        }
        if (SceneManager.GetActiveScene().name == "Level2") {
            pointsToAdd2 = 12;
        }
        if (SceneManager.GetActiveScene().name == "Level3") {
            pointsToAdd2 = 22;
        }
        Debug.Log(pointsToAdd2);
        Destroy(gameObject, 4);
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<Score>().AddScore(pointsToAdd2);
          
            Destroy(gameObject);


        }
    }
}
