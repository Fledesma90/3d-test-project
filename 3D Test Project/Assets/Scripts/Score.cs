using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Score : MonoBehaviour, IDataPersistance {
    public int score;
    public TextMeshProUGUI textScore;
    public TextMeshProUGUI scorePanelLvl1;
    public TextMeshProUGUI scorePanelLvl2;
    public TextMeshProUGUI scorePanelLvl3;
    public TextMeshProUGUI scoreGameOver;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textScore.text = score.ToString();
        scorePanelLvl1.text = score.ToString();
        scorePanelLvl2.text = score.ToString();
        scorePanelLvl3.text = score.ToString();
        scoreGameOver.text = score.ToString();
    }
    public void LoadData(GameData data) {
        this.score = data.score;
    }

    public void SaveData(ref GameData data) {
        data.score = this.score;
    }
    public void AddScore(int scoreToAdd) {
        score += scoreToAdd;
    }

    public void SubstractScore(int scoreToSubstract) {
        if (score>0) {
            score -= scoreToSubstract;
        }
        
        if (score<=0) {
            score = 0;
        }
    }
}
