using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float velocity;
    [SerializeField] float normalVelocity;
    private Rigidbody RB;
    private CapsuleCollider capsuleCollider;
    public GameObject gameOverGO;
    // Start is called before the first frame update
    void Start()
    {
        velocity = normalVelocity;
        capsuleCollider = GetComponent<CapsuleCollider>();
        RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate() {
        Movement();
    }
    void Movement() {
        float inputH = Input.GetAxisRaw("Horizontal");
        float inputV = Input.GetAxisRaw("Vertical");
        RB.velocity = new Vector3(inputH * velocity * Time.deltaTime, RB.velocity.y, inputV * velocity * Time.deltaTime);
    }

    public void GameOver() {
        velocity = 0;
        gameOverGO.SetActive(true);
    }
}
