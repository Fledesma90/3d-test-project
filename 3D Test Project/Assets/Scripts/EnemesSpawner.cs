using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemesSpawner : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InstantiateEnemies());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator InstantiateEnemies() {
        while (true) {
            Instantiate(enemyPrefab, gameObject.transform.position, gameObject.transform.rotation);
            yield return new WaitForSeconds(5);
        }
    }
}
