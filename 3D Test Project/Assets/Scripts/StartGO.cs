using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGO : MonoBehaviour
{
    [SerializeField] GameObject powerUpSpawner;
    [SerializeField] GameObject enemyPrefabSpawner;
    Timer timer;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }
   
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            timer = FindObjectOfType<Timer>();
           
            timer.GetComponent<Timer>().timerOn = true;
    
            powerUpSpawner.SetActive(true);
            enemyPrefabSpawner.SetActive(true);
            Destroy(gameObject);
        }
    }
}
