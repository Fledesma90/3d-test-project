using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class DataPersistanceManager : MonoBehaviour
{
    [Header("File Store Config")]
    [SerializeField] private string fileName;
    //We want to track gameData status
    private GameData gameData;
    private List<IDataPersistance> dataPersistanceObjects;
    private FileDataHandler dataHandler;
    //Singleton
    public static DataPersistanceManager instance { get; private set; }
    private void Awake() {
        if (instance != null) {
            Debug.LogError("Found more than one of this in the scene");
        }
        instance = this;
    }

    private void Start() {
        this.dataHandler = new FileDataHandler(Application.persistentDataPath, fileName);
        this.dataPersistanceObjects = FindAllDataPersistanceObjects();
        LoadGame();
    }
    //Methods that can be called anywhere
    public void NewGame() {
        this.gameData = new GameData();
    }
    public void LoadGame() {
        this.gameData = dataHandler.Load();
        if (this.gameData == null) {
            Debug.Log("no data was found");
            NewGame();
        }
        foreach (IDataPersistance dataPersistanceObj in dataPersistanceObjects) {
            dataPersistanceObj.LoadData(gameData);
        }

    }
    public void SaveGame() {
        foreach (IDataPersistance dataPersistanceObj in dataPersistanceObjects) {
            dataPersistanceObj.SaveData(ref gameData);
        }

        dataHandler.Save(gameData);
    }

    private void OnApplicationQuit() {
        SaveGame();
       
    }

    private List<IDataPersistance> FindAllDataPersistanceObjects() {
        IEnumerable<IDataPersistance> dataPersistanceObjects = FindObjectsOfType<MonoBehaviour>()
            .OfType<IDataPersistance>();

        return new List<IDataPersistance>(dataPersistanceObjects);
    }
}
