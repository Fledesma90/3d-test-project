using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PowerUpType1 : MonoBehaviour
{
    [SerializeField] int pointsToAdd1;
    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name=="Level1") {
            pointsToAdd1 = 1;
        }
        if (SceneManager.GetActiveScene().name == "Level2") {
            pointsToAdd1 = 10;
        }
        if (SceneManager.GetActiveScene().name == "Level3") {
            pointsToAdd1 = 20;
        }
        Debug.Log(pointsToAdd1);
        Destroy(gameObject, 6);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<Score>().AddScore(pointsToAdd1);
           
            Destroy(gameObject);


        }
    }
}
