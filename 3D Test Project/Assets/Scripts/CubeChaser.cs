using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class CubeChaser : MonoBehaviour
{
    //target al que perseguirá el enemigo
    Transform target;
    //referencia al componente navmesh agent
    NavMeshAgent navMeshAgent;

    float timerAttacking;
    bool canCount;

    // Start is called before the first frame update
    void Start()
    {
        //referencia al componente navmeshagent en el start
        navMeshAgent = GetComponent<NavMeshAgent>();
        target = FindObjectOfType<PlayerMovement>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        navMeshAgent.SetDestination(target.position);
        if (canCount==true) {
            timerAttacking += Time.deltaTime;
        }
        if (timerAttacking>=5) {
            target.GetComponent<PlayerMovement>().GameOver();
        }

        if (canCount==false) {
            timerAttacking = 0;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.GetComponent<PlayerMovement>()) {
         
            target.GetComponent<Score>().SubstractScore(2);
            canCount = true;
        }
    }

    private void OnCollisionExit(Collision collision) {
        
        if (collision.gameObject.GetComponent<PlayerMovement>()) {
            canCount = false;
           
        }
    }
}
