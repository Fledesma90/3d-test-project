using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Timer : MonoBehaviour, IDataPersistance {
    int time;
    float timeFloat;
    //reference to text element in which count is going to appear on UI
    public TextMeshProUGUI textTimer;
    public TextMeshProUGUI resultPanelLvl1;
    public TextMeshProUGUI resultPanelLvl2;
    public TextMeshProUGUI resultPanelLvl3;
    public TextMeshProUGUI resultGameOver;
    public bool timerOn;
    public void LoadData(GameData data) {
        this.time = data.time;
    }

    public void SaveData(ref GameData data) {
        data.time = this.time;
    }
    // Start is called before the first frame update
    void Start() {
        timeFloat = Mathf.RoundToInt(time);
        //initialize time to 0
        time = 0;
    }

    // Update is called once per frame
    void Update() {
        if (timerOn==true) {
            timeFloat += 1*Time.deltaTime;
            if (timeFloat > time) {
                time = Mathf.RoundToInt(timeFloat);
            }
        }
        textTimer.text = time.ToString();
        resultPanelLvl1.text= time.ToString();
        resultPanelLvl2.text = time.ToString();
        resultPanelLvl3.text = time.ToString();
        resultGameOver.text = time.ToString();
    }
    


}
