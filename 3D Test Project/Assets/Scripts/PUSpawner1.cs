using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUSpawner1 : MonoBehaviour
{
    [SerializeField] int randomNum;
    [SerializeField] GameObject powerUp;
    [SerializeField] GameObject [] spawnPoints;

    private void Awake() {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InstantiatePowerUps());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator InstantiatePowerUps() {
        while (true) {
            randomNum = Random.Range(0, 4);
            TransformPowerUpSelector();
            yield return new WaitForSeconds(10);
        }
    }

    private void TransformPowerUpSelector() {
        if (randomNum == 0) {
            Transform[] transformList = spawnPoints[0].GetComponentsInChildren<Transform>();
            foreach (Transform transform in transformList) {
                Instantiate(powerUp, transform.position,transform.rotation);
            }
        }
        if (randomNum == 1) {
            Transform[] transformList = spawnPoints[1].GetComponentsInChildren<Transform>();
            foreach (Transform transform in transformList) {
                Instantiate(powerUp, transform.position, transform.rotation);
            }
        }
        if (randomNum >= 2) {
            Transform[] transformList = spawnPoints[2].GetComponentsInChildren<Transform>();
            foreach (Transform transform in transformList) {
                Instantiate(powerUp, transform.position, transform.rotation);
            }
        }
    }
}
