using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishRoundFX : MonoBehaviour
{
    PlayerMovement playerMovement;
    GameObject[] enemies;
    EnemesSpawner enemesSpawner;
    GameObject []pUSpawner1;
    Timer timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = FindObjectOfType<Timer>();
        timer.GetComponent<Timer>().timerOn = false;
        playerMovement = FindObjectOfType<PlayerMovement>();
        playerMovement.gameObject.GetComponent<PlayerMovement>().enabled = false;
        enemesSpawner = FindObjectOfType<EnemesSpawner>();
        enemesSpawner.gameObject.SetActive(false);
        pUSpawner1 = GameObject.FindGameObjectsWithTag("PowerSpawn");
        foreach (GameObject powerupspawn in pUSpawner1) {
            powerupspawn.SetActive(false);
        }
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies ){
            Destroy(enemy);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
