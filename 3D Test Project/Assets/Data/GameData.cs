using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class GameData 
{
    //almacenamos las veces que hacemos click
    public int touchedGO;
    public int score;
    public int time;
    //creamos constructor y lo igualamos a 0, la idea aquí es que cuando empezamos
    //un JUEGONUEVO lo que pongamos en el constructor será el valor inicial con lo que
    //vamos a empezar
    public GameData() {
        this.touchedGO = 0;
        this.score = 0;
        this.time = 0;
    }
}
