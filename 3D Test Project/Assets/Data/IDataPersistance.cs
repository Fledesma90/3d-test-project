using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDataPersistance 
{
    void LoadData(GameData data);
    //Reference needed becasue when we save data, we want to
    //allow the implemented script to modify the data, so
    //LOAD only is in charge of reading while save we can modify
    void SaveData(ref GameData data);
}
